import { Pagination, PaginationProps } from "antd"
import "./index.less"

const CommonPagination = (props: PaginationProps) => {
  const { pageSizeOptions = [100, 300, 500] } = props
  return <div className="common-pagination-box">
    <Pagination
      className="common-pagination"
      showSizeChanger
      showQuickJumper
      showTotal={(total) => `共${total}条`}
      pageSizeOptions={pageSizeOptions}
      {...props}
    // showTotal={(total: number, range: [number, number]) => {
    //   return <div className="total-box">
    //     <span className="total">{`共${props.total || total}条`}</span>
    //     <div className="size-change-box">
    //       每页
    //       <Select className="size-change-select" value={props.pageSize} options={pageSizeOptions.map((item) => ({ value: item, label: item }))} onSelect={value => props.onChange && props.onChange(props.current || 1, value)}></Select>
    //       条
    //     </div>
    //   </div>;
    // }} 
    />
  </div>
}

// const CommonPagination = (props: PaginationProps) => {
//   const { sizeOptions = [100, 300, 500] } = props
//   return <div className="common-pagination-box">
//     <Pagination showTotal
//       showJumper
//       sizeCanChange
//       {...props}
//       sizeOptions={sizeOptions} />
//   </div>
// }

export default CommonPagination