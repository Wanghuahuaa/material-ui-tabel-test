import { TableCell } from "@mui/material";
import { Checkbox, Col, Row } from "antd";
import { ExpandableConfig } from "antd/es/table/interface";
import React, { Key, MouseEventHandler, ReactNode } from "react";
import { getTableExpandIcon } from "../common";
import { CHECK_TYPE, IMUIColumnsType, ITableRowSelection } from "./common";
import { getNewExpandKeys } from "./useTable";

interface IRowContent {
    (
        row: any,
        data: any[],
        rowKey: string,
        totalCol: IMUIColumnsType,
        expandedRowKeys: Key[],
        onExpandedRowsChange: (newKeys: Key[]) => void,
        expandable?: ExpandableConfig<any>,
        rowSelection?: ITableRowSelection,  // 复选框参数
    ): ReactNode
}

const rowContent: IRowContent = (row, data, rowKey, totalCol, expandedRowKeys, onExpandedRowsChange, expandable, rowSelection) => {
    // const { row, expandable, rowSelection, totalCol, expandedRowKeys, rowKey, columnWidths, onExpandedRowsChange, data } = props
    const { columnWidth, getRowChecked, onRowCheck } = rowSelection || {}
    const selectionColWidth = rowSelection ? Number(columnWidth) : 0
    const totalColCount = selectionColWidth ? totalCol.length + 1 : totalCol.length
    const { row_level, row_Type, row_index } = row
    const { rowExpandable, indentSize = 20 } = expandable || {}
    const indentIndex = 0
    let canExpand = false
    if (expandable) {
        if (rowExpandable ? rowExpandable(row) : true) canExpand = true
    } else if (row.children?.length) {
        canExpand = true
    }

    const onCheck: MouseEventHandler<HTMLElement> = (e) => {
        e.stopPropagation()
        onRowCheck?.(row)
    }

    // 自定义展开行
    if (row_Type === 1) return <TableCell colSpan={totalColCount}>
        {
            expandable?.expandedRowRender?.(row, 0, 0, expandedRowKeys?.includes(row[rowKey]))
        }
    </TableCell>
    return (
        <React.Fragment>
            {
                !!rowSelection && (<TableCell
                    style={{
                        minWidth: selectionColWidth,
                        width: selectionColWidth,
                        maxWidth: selectionColWidth,
                    }}
                    sx={{
                        backgroundColor: 'background.paper',
                    }}
                    className="selection-col"
                >
                    <Checkbox indeterminate={getRowChecked?.(row) === CHECK_TYPE.HALF_CHECKED} checked={getRowChecked?.(row) === CHECK_TYPE.CHECKED} onClick={onCheck}></Checkbox>
                </TableCell>)
            }
            {totalCol.map((column, index) => {
                const { dataIndex, children, ellipsis, render, width, stretchDisabled, sorter, sortOrder, onCell, ...rest } = column
                const cellProps = onCell?.(row, row_index) || {}
                // const { align, ...restCellProps } = cellProps
                // 这里也要限制最大宽度(tbody宽度设为自适应时 如果只限制th的maxWidth td内容超出时宽度也会自适应)
                const widthStyle = width ? {
                    width: width,
                    minWidth: width,
                    maxWidth: width
                } : {}
                return (
                    <TableCell
                        key={column.dataIndex}
                        title={ellipsis ? row[dataIndex] : undefined}
                        sx={{
                            backgroundColor: 'background.paper',
                        }}
                        style={widthStyle}
                        {...rest}
                        {...cellProps}
                    >
                        <Row className='size100' align="middle" wrap={false}>
                            <Col flex={"none"}>
                                {/* 子级缩进 */}
                                {row_level > 0 && indentIndex === index && <div style={{ width: row_level * indentSize, height: "100%" }}></div>}
                                {/* 展开收起图标 */}
                                {canExpand && indentIndex === index && getTableExpandIcon(
                                    {
                                        expanded: expandedRowKeys.includes(row[rowKey]),
                                        onExpand: (record: any) => onExpandedRowsChange(getNewExpandKeys(expandedRowKeys, record, rowKey)),
                                        record: row,
                                    },
                                    data,
                                    rowKey,
                                )}
                            </Col>
                            <Col flex={"auto"} className="text_over_flow">
                                {render ? render(row[dataIndex], row, row_index) : row[dataIndex]}
                            </Col>
                        </Row>
                    </TableCell>
                )
            })}
        </React.Fragment>
    );
}

export default rowContent

