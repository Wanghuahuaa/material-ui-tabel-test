import { TableCell, TableRow } from "@mui/material";
import { Checkbox } from "antd";
import { MouseEventHandler } from "react";
import { getArrLevel } from "../../../common/utils";
import { CHECK_TYPE, IMUIColumnType, IMUIColumnsType, ITableRowSelection } from "../common";
import ResizableTitle from "./ResizableTitle";

const getColArr = (columns: IMUIColumnsType) => {
    const totalLevel = getArrLevel(columns)
    const cols: any[] = Array(totalLevel).fill("_").map((_) => [])
    const v = (arr: IMUIColumnsType, level: number = 0) => {
        arr.forEach((col: IMUIColumnType) => {
            cols[level].push({
                ...col,
                headerColSpan: col?.children?.length || 1,
                headerRowSpan: !col?.children?.length ? totalLevel - level : 1,
            })
            if (col?.children?.length) v(col?.children, level + 1)
        })
    }
    v(columns)
    return cols
}

export const fixedHeaderContent = (showColumns: IMUIColumnsType, handleResize: any, resizeDisable: boolean | undefined, rowSelection: ITableRowSelection | undefined, onSortChange: (column: IMUIColumnType) => void) => {
    // const [columnWidths, setColumnWidths] = useState<any>(getNewColsWidth(columns));
    const cols = getColArr(showColumns)
    const selectionColWidth = rowSelection ? rowSelection?.columnWidth : 0
    const { getRowChecked, onRowCheck } = rowSelection || {}

    const onCheck: MouseEventHandler<HTMLElement> = (e) => {
        e.stopPropagation()
        onRowCheck?.()
    }

    return cols.map((colRow: any, i: number) => {
        return <TableRow key={i}>
            {
                i === 0 && !!rowSelection && (<TableCell
                    variant="head"
                    rowSpan={cols.length + 1}
                    style={{
                        minWidth: selectionColWidth,
                        width: selectionColWidth,
                        maxWidth: selectionColWidth,
                    }}
                    sx={{
                        backgroundColor: 'background.paper',
                    }}
                    className="selection-col"
                // {...rest}
                >
                    <Checkbox indeterminate={getRowChecked?.() === CHECK_TYPE.HALF_CHECKED} checked={getRowChecked?.() === CHECK_TYPE.CHECKED} onClick={onCheck}></Checkbox>
                </TableCell>)
            }
            {colRow.map((column: any) => {
                return (
                    <ResizableTitle
                        // height={0}
                        resizeDisable={resizeDisable}
                        column={{
                            ...column,
                            width: column.width,
                        }}
                        // style={{ top: i * 33 }}
                        key={column.dataIndex}
                        onResize={(e, { size }) => {
                            handleResize(column, size.width)
                        }}
                        onSortChange={onSortChange}
                    >
                        {column.label}
                    </ResizableTitle>
                )
            })}
        </TableRow>
    })
}