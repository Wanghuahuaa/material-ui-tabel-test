import { TABLE_COL_MIN_WIDTH } from "../common"

// 获取列总宽度
export const getColumnsTotalWidth = (cols: any[] = [], columnWidths: any = {}, filter?: (col: any) => boolean): number => {
    return cols.reduce((pre: number, col: any) => {
        const { dataIndex, width, children } = col
        const w = columnWidths[dataIndex] || width || TABLE_COL_MIN_WIDTH
        if (children?.length > 0) return pre + getColumnsTotalWidth(children, filter)
        if (filter && !filter(col)) return pre
        return pre + w
    }, 0)
}

