import { Table, TableHead, TableRow } from '@mui/material';
import Paper from '@mui/material/Paper';
import TableBody from '@mui/material/TableBody';
import TableContainer from '@mui/material/TableContainer';
import * as React from 'react';
import { TableComponents, TableVirtuoso } from 'react-virtuoso';
import CommonPagination from '../CommonPagination';
import { DEFAULT_CHILD_FIELD_NAME, DEFAULT_ROW_KEY, IMUITable, getTableRowsData, getTotalCol } from './common';
import "./index.less";
import rowContent from './rowConent';
import { fixedHeaderContent } from './useResize/ResizeHeader';
import useTableResize from './useResize/useResize';
import { useTable } from './useTable';

const MUITable = (props: IMUITable) => {
    const { columns,
        data,
        pagination,
        rowKey = DEFAULT_ROW_KEY,
        expandable,
        getRowDisabled,
        selRow,
        resizeDisable,
        childFieldName = DEFAULT_CHILD_FIELD_NAME,
        rowClassName,
        ...tableRest
    } = props
    const { expandedRowKeys, onExpandedRowsChange, onSelectRow, rowSelection, onSortChange } = useTable(props)
    const { useResize, wrapRef, showColumns, handleResize } = useTableResize(columns, Number(rowSelection?.columnWidth || 0), data)
    // 所有列(将子级列放出来)
    const totalCol = getTotalCol(showColumns)

    // console.log(25, columns, showColumns)

    const VirtuosoTableComponents: TableComponents<any> = {
        Scroller: MyTableContainer,
        Table: MyTable,
        TableHead,
        TableRow: ({ item: _item, ...props }) => {
            return (<TableRow
                className={`${rowClassName?.(_item, _item.row_index)} ${selRow?.[rowKey] === _item[rowKey] ? "table-selected-row" : ""}`}
                onClick={() => onSelectRow(_item)}
                {...props}
            />)
        },
        TableBody: MyTableBody,
        // ...tableRest,
    }

    const showData = React.useMemo(() => {
        return getTableRowsData(data, expandable, expandedRowKeys, rowKey, childFieldName)
    }, [data, expandedRowKeys])

    return (
        <div style={{ padding: 100 }} className='size100 mui-table-wrapper'>
            <div className={`size100 table-box ${pagination ? "table-with-pagination" : ""}`} ref={wrapRef}>
                <TableVirtuoso
                    className={`mui-table ${useResize ? "resize-table" : ""}`}
                    data={showData}
                    components={VirtuosoTableComponents}
                    fixedHeaderContent={() => fixedHeaderContent(showColumns, handleResize, resizeDisable, rowSelection, onSortChange)}
                    itemContent={(_index: number, row: any) => rowContent(row, data, rowKey, totalCol, expandedRowKeys, onExpandedRowsChange, expandable, rowSelection)}
                />
            </div>
            {pagination && <CommonPagination {...pagination} />}
        </div>
    );
}

export default MUITable

const MyTable = (props: any) => (
    <Table {...props} sx={{ borderCollapse: 'separate', tableLayout: 'fixed' }} />
)

const MyTableContainer = React.forwardRef<HTMLDivElement>((props, ref) => (
    <TableContainer component={Paper} {...props} ref={ref} />
))

const MyTableBody = React.forwardRef<HTMLTableSectionElement>((props, ref) => {
    return (
        <TableBody {...props} ref={ref} />
    )
})