import { TableCellProps } from "@mui/material";
import { TablePaginationConfig } from "antd";
import { ExpandableConfig, FilterValue, Key, SortOrder, SorterResult, TableRowSelection } from "antd/es/table/interface";
import { TableComponents } from "react-virtuoso";

export interface IMUIColumnType extends Omit<TableCellProps, "children"> {
    dataIndex: string;
    label: React.ReactNode;
    width?: number;  // 有children的合并表头不需要 其它情况必传
    children?: IMUIColumnsType;
    ellipsis?: boolean,
    render?: (value: any, record: any, index: number | string) => React.ReactNode;
    stretchDisabled?: boolean,  // 计算实际渲染宽度时 不允许列拉伸
    sorter?: boolean | {
        multiple?: boolean | number;
    };
    sortOrder?: SortOrder;
    // rowSpan?: number,
    // colSpan?: number,
    onCell?: (row: any, index?: number) => React.HTMLAttributes<any> & React.TdHTMLAttributes<any>,
}

export type IMUIColumnsType = IMUIColumnType[];
export interface IMUITable extends TableComponents {
    columns: IMUIColumnsType;
    data: any[],
    pagination?: false | TablePaginationConfig;
    rowKey?: string;
    expandable?: ExpandableConfig<any>;
    getRowDisabled?: (record: any) => boolean; //表格行是否可选中
    selRow?: any; //选中的行
    setSelRow?: (record: any) => void;
    rowSelection?: TableRowSelection<any>;  // 复选框参数
    resizeDisable?: boolean; // 禁用表头拖拽
    childFieldName?: string;  // 子级取哪个字段
    rowClassName?: (record: any, index: number) => string  // 设置行样式
    // 这里只有排序的变化会被监听到(都写上是为了和antd的表格的onChange参数保持一致)
    onChange?: (pagination: TablePaginationConfig | undefined, filters: Record<string, FilterValue | null> | undefined, sorter: SorterResult<any> | SorterResult<any>[]) => void;
}

export enum CHECK_TYPE {
    NOT_CHECKED = 0,
    CHECKED = 1,
    HALF_CHECKED = 2,
}

export interface ITableRowSelection extends TableRowSelection<any> {
    getRowChecked?: (row?: any) => CHECK_TYPE;  // 不传row表示全选
    onRowCheck?: (row?: any) => void;  // // 不传row表示全选
}

export const DEFAULT_ROW_KEY = "tid"

export const DEFAULT_CHILD_FIELD_NAME = "children"

// 列最小宽度 
export const TABLE_COL_MIN_WIDTH = 50

// 复选框列的宽度 
export const SELECTION_COL_WITH = 50

// 滚动条宽度
export const SCROLLBAR_WIDTH = 8

// 自定义展开行内容key
export const CUSTOM_EXPAND_ROW_KEY = "_CUSTOM_EXPAND_ROW"

export const getTotalCol = (columns: IMUIColumnsType): IMUIColumnsType => {
    return columns.reduce((pre: IMUIColumnsType, item: any) => {
        if (item.children?.length) return [...pre, ...getTotalCol(item.children)]
        return [...pre, item]
    }, [])
}

export const getTotalColCount = (columns: IMUIColumnsType): number => {
    return columns.reduce((pre: number, item: any) => {
        if (item.children?.length) return pre + getTotalColCount(item.children)
        return pre + 1
    }, 0)
}


export const getTableRowsData = (data: any[], expandable: ExpandableConfig<any> = {}, expandedRowKeys: Key[] = [], rowKey: string, childFieldName: string = DEFAULT_CHILD_FIELD_NAME) => {
    const { expandedRowRender } = expandable
    const rows: any[] = []
    const v = (arr: any[], rowLevel = 0) => {
        arr.forEach((item: any, index) => {
            rows.push({
                ...item,
                row_level: rowLevel,
                row_index: index,
            })
            // 展开的子级
            if (item[childFieldName]?.length && expandedRowKeys.includes(item[rowKey])) v(item[childFieldName], rowLevel + 1)
            // 自定义展开项
            if (expandedRowRender && expandedRowKeys.includes(item[rowKey])) rows.push({
                ...item,
                [rowKey]: item[rowKey] + CUSTOM_EXPAND_ROW_KEY,
                // 表示是自定义展开行
                row_Type: 1
            })
        })
    }
    v(data)
    return rows
}
