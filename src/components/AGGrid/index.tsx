"use strict";

import { GetRowIdParams, RowClassParams, RowClickedEvent } from "ag-grid-community";
import "ag-grid-community/styles/ag-grid.css";
import "ag-grid-community/styles/ag-theme-quartz.css";
import "ag-grid-enterprise";
import { GetDataPath } from "ag-grid-enterprise";
import { AgGridReact } from "ag-grid-react";
import { forwardRef, useCallback, useImperativeHandle, useMemo, useRef } from "react";
import { DEFAULT_ROW_KEY, IAGTable } from "./const";
import "./index.less";

const AGTable = forwardRef((props: IAGTable, ref) => {
    const { rowData, columnDefs, selRow, setSelRow, rowKey = DEFAULT_ROW_KEY, ...rest } = props
    const tableRef = useRef<AgGridReact<any>>(null)

    useImperativeHandle(ref, () => {
        return {}
    })

    const getDataPath = useMemo<GetDataPath>(() => {
        return (data: any) => {
            console.log(24, data)
            return data.children;
        };
    }, []);

    const onRowClicked = useCallback((params: RowClickedEvent<any, any>) => {
        setSelRow(params.data)
    }, [])

    // 自定义列类型
    const columnTypes = useMemo(() => {
        return {
            // 列居中对齐
            centerAligned: {
                headerClass: 'center-aligned-header',
                cellClass: 'center-aligned-cell'
            },
            // 带Title的列 重写cellRender后无效
            cellWithTitle: {
                cellRenderer: (params: any) => {
                    return <div className="w100 text_over_flow" title={params.value}> {params.value} </div>;
                },
            },
        };
    }, []);

    const getRowClass = useMemo(() => {
        return (params: RowClassParams<any, any>) => {
            if (selRow && params.data[rowKey] === selRow[rowKey]) return `table-selected-row`
        }
    }, [selRow, rowData])

    const getRowId = useMemo(() => {
        return (params: GetRowIdParams) => params.data[rowKey];
    }, [])

    return (
        <div className="ag-table-container">
            <div className={"ag-theme-quartz ag-table-box"}>
                <AgGridReact
                    ref={tableRef}
                    rowData={rowData}
                    columnDefs={columnDefs}
                    // onGridReady={onGridReady}
                    autoSizeStrategy={{
                        type: "fitGridWidth",
                    }}
                    suppressRowClickSelection
                    rowSelection="multiple"
                    groupSelectsChildren={true}
                    getRowClass={getRowClass}
                    getRowId={getRowId}
                    // debounceVerticalScrollbar
                    // 树形数据
                    // treeData
                    // 树形路径 需要有一个字段存 string[] 表示每一行数据的路径
                    // getDataPath={getDataPath}
                    // suppressColumnVirtualisation
                    // animateRows={false}
                    // enableCellChangeFlash={false}
                    // 行点击事件
                    onRowClicked={onRowClicked}
                    // 自定义渲染
                    // detailCellRenderer={detailCellRenderer}
                    // 自定义展开内容行高自适应
                    //   detailRowAutoHeight={true}
                    // 自定义展开内容 设置高度
                    // detailRowHeight={200}
                    columnTypes={columnTypes}
                    scrollbarWidth={8}
                    // alwaysShowVerticalScroll
                    suppressPreventDefaultOnMouseWheel
                    {...rest}
                />
            </div>
        </div>
    );
});

export default AGTable
