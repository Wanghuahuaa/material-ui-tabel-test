import { GridOptions } from "ag-grid-enterprise";

export interface IAGTable extends GridOptions {
    selRow?: any;
    setSelRow?: any;
    rowKey?: string;
}

export const DEFAULT_ROW_KEY = "tid"
