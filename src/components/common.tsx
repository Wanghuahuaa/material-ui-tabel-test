// import tableIconCollapse from "@/assets/svgs/tableIconCollapse.svg";
// import tableIconExpand from "@/assets/svgs/tableIconExpand.svg";
import { CaretDownOutlined, CaretUpOutlined, MinusOutlined, PlusOutlined } from "@ant-design/icons";
import { Row } from "antd";
import { SortOrder } from "antd/es/table/interface";


export const getTableExpandIcon = (props: any, dataSource: any[], rowKey: string = "tid") => {
  // 每一项都没有子级时
  if (dataSource?.length && !dataSource?.some((item: any) => item.children?.length > 0)) return <></>;
  const { expanded, onExpand, record } = props;
  const pathInfo = getRowPath(record, dataSource, rowKey)
  // 缩进占位元素(每一个上级都有一根线)
  const indentItems = pathInfo.map((info: any, i: number) => {
    // showHalf = isLastChild && (!record.children?.length || !expanded)
    const { isLastChild, fid } = info
    // if (record.mc == "工程管理部" && i == 0) console.log(pathInfo, info, isLastChild, expandedRowKeys.includes(fid))
    // const showHalf = isLastChild && (!expandedRowKeys.includes(fid) || !record.children?.length || !expanded)
    const showHalf = isLastChild && (!record.children?.length || !expanded)
    return (<div key={i} className={`table-row-indent level-${i} ${showHalf ? "showHalf" : ""}`}></div>)
  })
  if (!record.children?.length) return <>
    {/* {indentItems} */}
    <span style={{ marginRight: 20 }}></span>
  </>;
  return <>
    {/* {indentItems} */}
    {/* {
      expanded ? (
        <ReactSVG src={tableIconCollapse} className="react-svg pointer table-row-expand-icon collapse" onClick={(e) => onExpand(record, e)}></ReactSVG>
      ) : (
        <ReactSVG src={tableIconExpand} className="react-svg pointer table-row-expand-icon expand" onClick={(e) => onExpand(record, e)}></ReactSVG>
      )
    } */}
    {
      expanded ? (
        <MinusOutlined className="table-switch-icon" onClick={(e) => onExpand(record, e)} />
      ) : (
        <PlusOutlined className="table-switch-icon" onClick={(e) => onExpand(record, e)} />
      )
    }
  </>
};

// 获取行的路径 并判断路径上的每一行是否所在层最后一行 （去掉第一层的）
const getRowPath = (record: any, dataSource: any[], rowKey: string) => {
  let pathInfo: any = []
  const v = (list: any[], p: any[] = []) => {
    list.map((item: any, i) => {
      const isLastChild = i === list.length - 1
      const mewPath = [
        ...p,
        {
          fid: item.tid,
          isLastChild,
        }
      ]
      if (item[rowKey] === record[rowKey]) {
        pathInfo = [...mewPath]
        return
      }
      if (item.children?.length) v(item.children, mewPath)
    })
  }
  v(dataSource)
  return pathInfo.slice(1)
}


export const getTableSortIcon = (sortOrder: SortOrder) => {
  return <Row className="table-sort-box">
    <CaretUpOutlined className={"react-svg " + (sortOrder === "ascend" ? "active" : "")} />
    <CaretDownOutlined className={"react-svg " + (sortOrder === "descend" ? "active" : "")} />
  </Row>
}