"use strict";

import "ag-grid-community/styles/ag-grid.css";
import "ag-grid-community/styles/ag-theme-quartz.css";
import "ag-grid-enterprise";
import { ColDef, ColGroupDef } from "ag-grid-enterprise";
import { useEffect, useMemo, useRef, useState } from "react";
import AGTable from "../../components/AGGrid";
import { getData } from "./data";
import "./index.less";

const AGGridTest = () => {
    const [tableData, setTableData] = useState<any[]>(getData())
    const [selRow, setSelRow] = useState<any[]>([])
    const tableRef = useRef<any>()

    useEffect(() => {
        setTimeout(() => {
            setTableData(getData())
        }, 1000)

        // setTimeout(() => {
        //     setTableData(getData().slice(0, 2))
        // }, 5000)
    }, [])

    const [columnDefs, setColumnDefs] = useState<(ColDef<any> | ColGroupDef<any>)[]>([
        {
            headerName: "",
            field: "selection_col",
            minWidth: 50,
            maxWidth: 50,
            headerCheckboxSelection: true,
            checkboxSelection: true,
            showDisabledCheckboxes: true,
        },
        // {
        //     headerName: "Row #",
        //     field: "rowNumber",
        //     width: 150,
        //     // pinned: "left",
        //     headerCheckboxSelection: true,
        //     checkboxSelection: true,
        //     showDisabledCheckboxes: true,
        //     // type: ["rightAligned"],
        //     // valueFormatter: (params) => {
        //     //     console.log(38, params.value)
        //     //     return params.value
        //     // }
        // },
        {
            field: "autoA",
            minWidth: 300,
            wrapText: true,
            autoHeight: true,
            headerName: "A) Auto Height",
            type: ["centerAligned", "cellWithTitle"],
            cellRenderer: (params: any) => {
                return <>Value is <b> {params.value} </b> </>;
            },
        },
        {
            width: 300,
            field: "autoB",
            wrapText: true,
            headerName: "B) Normal Height",
            children: [
                {
                    headerName: "child-col-1",
                    field: "child1",
                    width: 100,
                },
                {
                    headerName: "child-col-2",
                    field: "child2",
                    width: 100,
                },
            ],
        },
    ]);

    const detailCellRenderer = useMemo(() => {
        // return DetailCellRenderer;
    }, []);

    return (
        <div style={{ width: "100%", height: '100%', padding: '50px 200px' }}>
            <AGTable
                rowData={tableData}
                columnDefs={columnDefs}
                selRow={selRow}
                setSelRow={setSelRow}
            />
        </div>
    );
};

export default AGGridTest
