import { CaretDownOutlined, CaretRightOutlined } from "@ant-design/icons";
import { Button, Space, TableProps } from "antd";
import { Key, useEffect, useState } from "react";
import useTableSorter from "../../common/useSort";
import { priceFormatter } from "../../common/utils";
import MUITable from "../../components/MUITable";
import { IMUIColumnsType } from "../../components/MUITable/common";
import Expend from "./expend";
import { testData } from "./testData";

// 默认的分页参数
export const DEFAULT_PAGE_INFO = {
    current: 1,
    pageSize: 100,
    total: 0,
};

export const Test = () => {
    const [tableData, setTableData] = useState<any[]>([])
    const [pageInfo, setPageInfo] = useState<any>(DEFAULT_PAGE_INFO);
    const [checkedRows, setCheckedRows] = useState<any>([]);
    const [selRow, setSelRow] = useState<any>();
    const [sortInfo, getSortOrder, handleSortChange] = useTableSorter();
    const [recordId, setRecordId] = useState<Key[]>([]);

    useEffect(() => {
        setTableData(testData)
        setPageInfo({
            ...pageInfo,
            total: 555
        })
    }, [])

    //修改分页参数
    const onPageChange = (pageIndex: any, pageSize: any) => {
        // 切换pageSize时回到第一页
        if (pageSize !== pageInfo.pageSize) pageIndex = 1;
        setPageInfo({ ...pageInfo, current: pageIndex, pageSize: pageSize });
    };

    const rowSelection = {
        selectedRowKeys: checkedRows.map((item: any) => item.tid),
        onChange: (selectedRowKeys: React.Key[], selectedRows: any[]) => {
            //   selectRowKeysIdS.current = selectedRowKeys;
            setCheckedRows(selectedRows)
        },
        getCheckboxProps: (record: any) => ({
            disabled: true
        }),
    };

    const onExpandedRowsChange = (record: any) => {
        // console.log(3026, record)
        const i = (recordId || []).findIndex((item: any) => item === record.tid)
        const tmp = [...recordId]
        if (i !== -1) {
            tmp.splice(i, 1)
        } else {
            tmp.push(record.tid)
        }
        setRecordId(tmp)
    }

    // console.log(recordId)

    const columns: IMUIColumnsType = [
        {
            label: "项目名称",
            width: 390,
            dataIndex: "xmmc",
            ellipsis: true,
            align: "left",
            render(value, record, index) {
                return <div
                    className="text_over_flow"
                    // ref={expandDivRef}
                    datatype={`dataType-${record.tid}`}
                    style={{
                        display: "flex",
                        justifyContent: "start",
                    }}
                    onClick={(ev) => {
                        onExpandedRowsChange(record)
                    }}
                    title={record.xmmc}
                >
                    {record.xmfqs?.length ? (
                        recordId?.includes?.(record.tid) ? (
                            <CaretDownOutlined />
                        ) : (
                            <CaretRightOutlined />
                        )
                    ) : (
                        <></>
                    )}
                    &nbsp;&nbsp;&nbsp;
                    {record.xmmc}
                </div>
            },
        },
        {
            label: "项目编码",
            width: 100,
            dataIndex: "xmbm",
            ellipsis: true,
            align: "left",
            stretchDisabled: true,
        },
        {
            label: "项目类型",
            width: 105,
            dataIndex: "xmlx",
            ellipsis: true,
            align: "left",
            stretchDisabled: true,
        },
        {
            label: "项目性质",
            width: 105,
            dataIndex: "xmxz",
            ellipsis: true,
            align: "left",
            // stretchDisabled: true,
        },
        {
            label: "项目总投资",
            width: 105,
            dataIndex: "xmztz",
            align: "right",
            ellipsis: true,
            sorter: {
                multiple: 1,
            },
            sortOrder: getSortOrder("xmztz"),
        },
        {
            label: "动态成本",
            width: 105,
            dataIndex: "dtcbje",
            align: "right",
            ellipsis: true,
            sorter: {
                multiple: 2,
            },
            sortOrder: getSortOrder("dtcbje"),
        },
        {
            label: "单方造价(元/m2)",
            width: 130,
            dataIndex: "dtcbdfzj",
            align: "right",
            sorter: {
                multiple: 3,
            },
            sortOrder: getSortOrder("dtcbdfzj"),
            render(value, record, index) {
                return priceFormatter(value, "元", "");
            },
        },
        {
            label: "操作",
            width: 400,
            dataIndex: "cz",
            align: "center",
            // fixed: "right",
            stretchDisabled: true,
            render: (value: any, record: any, index: number | string) => {
                return (
                    <Space size={4}>
                        <Button type="link" >项目信息</Button>
                        <Button
                            type="link"
                        >
                            新建分期
                        </Button>
                        <Button type="link" >合同管理</Button>
                        <Button type="link" >项目人员</Button>
                        <Button type="link" >图纸管理</Button>
                    </Space>
                );
            },
        },
    ];

    //表格展开
    const expandableProps: TableProps<any>["expandable"] = {
        // columnWidth: 500,
        // childrenColumnName: "xmfqs",
        // rowExpandable: (record) => record.xmfqs?.length,
        expandedRowRender: (record) => {
            return (
                <div style={{ margin: "0 0 0 24px" }}>
                    <Expend
                        record={record}
                        getSortOrder={getSortOrder}
                        handleSortChange={handleSortChange}
                    />
                </div>
            );
        },
        // columnTitle: "项目名称",
        // expandIconColumnIndex: -1,
        showExpandColumn: false,
        // onExpandedRowsChange(expandedKeys) {
        //   setRecordId(expandedKeys);
        // },
        expandedRowKeys: recordId,
        // expandIcon: ({ prefixCls, expanded, onExpand, record }) => {
        //   return (
        //     <div
        //       className="text_over_flow"
        //       ref={expandDivRef}
        //       datatype={`dataType-${record.tid}`}
        //       // style={{
        //       //   display: "flex",
        //       //   justifyContent: "start",
        //       // }}
        //       onClick={(ev) => {
        //         record.xmfqs?.length ? onExpand(record, ev) : null;
        //       }}
        //       title={record.xmmc}
        //     >
        //       {record.xmfqs?.length ? (
        //         expanded ? (
        //           <CaretDownOutlined />
        //         ) : (
        //           <CaretRightOutlined />
        //         )
        //       ) : (
        //         <></>
        //       )}
        //       {/* &nbsp;&nbsp;&nbsp;
        //       {record.xmmc} */}
        //     </div>
        //   );
        // },
    };

    return <MUITable
        // data={testData.slice(0, 2)}
        data={tableData}
        columns={columns}
        pagination={{
            ...pageInfo,
            onChange: onPageChange,
        }}
        selRow={selRow}
        setSelRow={setSelRow}
        rowSelection={{
            ...rowSelection,
        }}
        expandable={expandableProps}
        onChange={handleSortChange}
    />
}