
import { Badge, Switch, Table } from "antd";
import type { ColumnsType } from "antd/es/table";
import { useRef } from "react";

interface Props {
  record: any;
  getSortOrder: any;
  handleSortChange: any;
}

export default function Expend(props: Props) {
  const {
    record,
    getSortOrder,
    handleSortChange,
  } = props;
  const selRowInfo = useRef<any>(); //选中行数据

  const columns: ColumnsType<any> = [
    {
      title: "合同名称",
      width: 500,
      dataIndex: "njhtmc",
      align: "left",
      key: "njhtmc",
      ellipsis: true,
    },
    {
      title: "合同编码",
      width: 120,
      dataIndex: "htbh",
      ellipsis: true,
      key: "htbh",
      align: "left",
    },
    {
      title: "合同类别",
      width: 120,
      dataIndex: "htlbmc",
      ellipsis: true,
      align: "left",
      key: "htlbmc",
    },
    {
      title: "采购方式",
      width: 120,
      dataIndex: "xcfs",
      ellipsis: true,
      key: "xcfs",
      align: "left",
    },
    {
      title: "计价方式",
      width: 120,
      dataIndex: "jjfs",
      ellipsis: true,
      key: "jjfs",
      align: "left",
    },
    {
      title: "合同金额",
      width: 120,
      dataIndex: "htje",
      ellipsis: true,
      key: "htje",
      align: "right",
      sorter: {
        // compare: (a, b) => a.htje - b.htje,
        multiple: 1,
      },
      sortOrder: getSortOrder("htje"),
    },
    {
      title: "合同状态",
      width: 120,
      dataIndex: "shzt",
      ellipsis: true,
      key: "shzt",
      align: "center",
      render: (value, record, index) => {
        return (
          <Badge
            status={value == 4 ? "error" : "success"}
            text={value == 4 ? "作废" : "正常"}
          />
        );
      },
    },
    {
      title: "清单状态",
      width: 120,
      dataIndex: "qdzt",
      ellipsis: true,
      key: "qdzt",
      align: "center",
      render: (value, record, index) => {
        return (
          <Switch
            checkedChildren={record.qdztmc}
            unCheckedChildren={record.qdztmc}
            checked={value != 1}
            onClick={() => {
            }}
          />
        );
      },
    },
    {
      title: "过控状态",
      width: 120,
      dataIndex: "gkzt",
      // ellipsis: true,
      key: "gkzt",
      render: (value, record, index) => {
        return (
          <Switch
            checkedChildren={record.gkztmc}
            unCheckedChildren={record.gkztmc}
            checked={value != 1}
            onClick={() => {
            }}
          />
        );
      },
    },
  ];

  console.log('expend render')

  return (
    <>
      {record.xmfqs?.map((item: any) => {
        return (
          <div className="expend" key={item.tid}>
            <div className="expend-top">
              555555555555555
            </div>
            <div style={{ color: "#1D2129" }}>
              <Table
                bordered={false}
                // virtual
                columns={columns}
                scroll={{ y: 800, x: 1000 }}
                rowKey="tid"
                dataSource={item.htlist}
                pagination={false}
                // rowClassName="h40"
                onChange={handleSortChange}
                onRow={(record) => {
                  return {
                    onClick: (e) => {
                      selRowInfo.current = record;
                    }, // 点击行
                  };
                }}
              />
            </div>
          </div>
        );
      })}
    </>
  );
}
