import { useEffect } from 'react'
import './App.less'
import AGGridTest from './views/AGGridTest'

function App() {

  useEffect(() => {
    // testAlert()
  }, [])

  return (
    <>
      {/* <ReactVirtualizedTable></ReactVirtualizedTable> */}
      {/* <Test></Test> */}
      <AGGridTest />
    </>
  )
}

export default App
